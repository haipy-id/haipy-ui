part of haipy_ui;

class HaipyHome extends StatelessWidget {
  const HaipyHome({
    Key key,
    @required this.home,
  }) : super(key: key);

  final Widget home;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 720,
      height: 1250,
      allowFontScaling: false,
    );

    return home;
  }
}
