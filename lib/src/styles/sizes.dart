part of haipy_ui;

class HaipySizes {
  HaipySizes._();

  static final smallText = ScreenUtil().setSp(24);
  static final regularText = ScreenUtil().setSp(32);
  static final mediumText = ScreenUtil().setSp(40);

  static num width(int width) {
    return ScreenUtil().setWidth(width);
  }

  static num height(int height) {
    return ScreenUtil().setHeight(height);
  }
}
