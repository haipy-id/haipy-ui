part of haipy_ui;

class HaipyGradients {
  HaipyGradients._();
  
  static LinearGradient blueGradient1 = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      HaipyColors.blue,
      HaipyColors.lightBlue,
    ],
  );

  static LinearGradient blueGradient2 = LinearGradient(
    begin: Alignment.centerRight,
    end: Alignment.centerLeft,
    colors: [
      HaipyColors.blue,
      HaipyColors.lightBlue,
    ],
  );
}
