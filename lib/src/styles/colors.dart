part of haipy_ui;

class HaipyColors {
  HaipyColors._();
  
  static const Color lightBlue = Color(0xFF2196F3);
  static const Color blue = Color(0xFF007AFF);
  static const Color darkBlue = Color(0xFF0064D0);

  static const Color teal = Color(0xFF17C1BB);

  static const Color purple = Color(0xFF455AA6);

  static const Color green = Color(0xFF09B627);

  static const Color grey = Color(0xFFE0E0E0);
}
