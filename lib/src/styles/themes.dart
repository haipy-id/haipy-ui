part of haipy_ui;

class HaipyThemes {
  HaipyThemes._();
  
  static final light = ThemeData(
    accentColor: HaipyColors.lightBlue,
    primaryColor: HaipyColors.lightBlue,
    primaryColorBrightness: Brightness.dark,
    toggleableActiveColor: HaipyColors.lightBlue,
    textTheme: GoogleFonts.sourceSansProTextTheme(),
  );

  static final dark = light.copyWith(
    brightness: Brightness.dark,
  );
}
