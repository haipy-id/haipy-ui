part of haipy_ui;

enum ButtonTextSize { smaller, normal, bigger }

class HaipyButton extends StatelessWidget {
  const HaipyButton({
    Key key,
    this.noMargin: false,
    this.outlineStyle: false,
    this.disabled: false,
    this.borderRadius,
    this.color,
    this.buttonTextSize: ButtonTextSize.normal,
    @required this.onTap,
    @required this.child,
  })  : assert(child != null, 'Child can\'t be null'),
        super(key: key);

  const HaipyButton.white({
    Key key,
    this.noMargin: false,
    this.outlineStyle: false,
    this.disabled: false,
    this.borderRadius,
    this.color: Colors.white,
    this.buttonTextSize: ButtonTextSize.normal,
    @required this.onTap,
    @required this.child,
  })  : assert(child != null, 'Child can\'t be null'),
        super(key: key);

  const HaipyButton.outline({
    Key key,
    this.noMargin: false,
    this.outlineStyle: true,
    this.disabled: false,
    this.borderRadius,
    this.color,
    this.buttonTextSize: ButtonTextSize.normal,
    @required this.onTap,
    @required this.child,
  })  : assert(child != null, 'Child can\'t be null'),
        assert(color == null, 'Color must be null for outline style'),
        super(key: key);

  final bool noMargin;
  final bool outlineStyle;
  final bool disabled;
  final BorderRadius borderRadius;
  final Color color;
  final ButtonTextSize buttonTextSize;
  final VoidCallback onTap;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    bool _disabled = disabled || onTap == null;
    BorderRadius _borderRadius = borderRadius ?? BorderRadius.circular(24.0);
    EdgeInsets _buttonPadding = EdgeInsets.symmetric(
      vertical: buttonTextSize == ButtonTextSize.smaller ? 4.0 : 8.0,
      horizontal: 16.0,
    );
    TextStyle _textStyle = buttonTextSize == ButtonTextSize.bigger
        ? TextStyle(fontSize: ScreenUtil().setSp(38))
        : buttonTextSize == ButtonTextSize.smaller
            ? TextStyle(fontSize: ScreenUtil().setSp(22))
            : TextStyle(fontSize: ScreenUtil().setSp(30));
    Color _color = outlineStyle
        ? Colors.transparent
        : color != null ? color : HaipyColors.grey;
    LinearGradient _gradient = _disabled
        ? null
        : outlineStyle
            ? null
            : color != null ? null : HaipyGradients.blueGradient1;

    return Container(
      margin: noMargin ? null : const EdgeInsets.all(8.0),
      child: RawMaterialButton(
        onPressed: !_disabled ? onTap : null,
        elevation: outlineStyle
            ? 0.0
            : buttonTextSize == ButtonTextSize.smaller ? 4.0 : 8.0,
        highlightElevation: 0.0,
        fillColor: _color,
        shape: RoundedRectangleBorder(
          side: outlineStyle
              ? BorderSide(
                  color: Colors.white,
                  width: 2.0,
                )
              : BorderSide.none,
          borderRadius: _borderRadius,
        ),
        constraints: const BoxConstraints(minHeight: 24.0, minWidth: 24.0),
        child: Container(
          alignment: Alignment.center,
          padding: _buttonPadding,
          decoration: BoxDecoration(
            gradient: _gradient,
            borderRadius: _borderRadius,
          ),
          child: DefaultTextStyle(
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .merge(_textStyle)
                .copyWith(
                  fontWeight: FontWeight.w400,
                  color: _disabled
                      ? Colors.grey.shade600
                      : color == Colors.white
                          ? HaipyColors.lightBlue
                          : Colors.white,
                ),
            child: child,
          ),
        ),
      ),
    );
  }
}
