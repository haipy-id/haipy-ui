part of haipy_ui;

class HaipyAppBar extends StatelessWidget {
  const HaipyAppBar({
    Key key,
    this.hideBackButton = false,
    this.backgroundColor,
    this.expandedHeight,
    this.actions,
    this.background,
    @required this.title,
  }) : super(key: key);

  final bool hideBackButton;
  final Color backgroundColor;
  final double expandedHeight;
  final List<Widget> actions;
  final Widget background;
  final Widget title;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      automaticallyImplyLeading: false,
      pinned: true,
      stretch: true,
      expandedHeight: background != null
          ? ScreenUtil().setHeight(expandedHeight ?? 300)
          : null,
      elevation: 0.0,
      leading: !hideBackButton && Navigator.of(context).canPop()
          ? IconButton(
              icon: Icon(Icons.chevron_left),
              onPressed: () => Navigator.maybePop(context),
            )
          : null,
      title: title != null
          ? DefaultTextStyle(
              child: title,
              style: DefaultTextStyle.of(context).style,
            )
          : const SizedBox(),
      actions: actions,
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(12.0),
        child: Container(
          alignment: Alignment.center,
          child: const SizedBox(),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.0),
              topRight: Radius.circular(16.0),
            ),
            color: backgroundColor != null
                ? Color.alphaBlend(
                    backgroundColor,
                    Theme.of(context).scaffoldBackgroundColor,
                  )
                : Theme.of(context).scaffoldBackgroundColor,
          ),
          padding: const EdgeInsets.all(6.0),
        ),
      ),
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: HaipyGradients.blueGradient2,
        ),
        child: background,
      ),
    );
  }
}
