part of haipy_ui;

class HaipyBottomTab {
  HaipyBottomTab({
    @required this.icon,
    @required this.tab,
    @required this.title,
  });

  final Widget icon;
  final Widget tab;
  final Widget title;
}

class BottomTabBar extends StatelessWidget {
  const BottomTabBar({
    Key key,
    this.index = 0,
    @required this.tabs,
    @required this.onTabPressed,
  }) : super(key: key);

  final int index;
  final List<HaipyBottomTab> tabs;
  final ValueChanged<int> onTabPressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: tabs.map((tab) {
        return Flexible(
          fit: FlexFit.tight,
          child: BottomTabItem(
            selected: tabs.indexOf(tab) == index,
            icon: tab.icon,
            label: tab.title,
            onTap: () => onTabPressed(tabs.indexOf(tab)),
          ),
        );
      }).toList(),
    );
  }
}

/*
modified from https://github.com/showang/flutter_multi_navigator_bottom_bar 
and https://gist.githubusercontent.com/HansMuller/b189642d10fd236a41044fdf7626f7b0/raw/8ef62fa066f0297f704ad339f0e74ce9422830a9/navigator_scroll_fade_demo.dart
*/

class BottomTabItem extends StatelessWidget {
  const BottomTabItem({
    Key key,
    @required this.selected,
    @required this.icon,
    @required this.label,
    @required this.onTap,
  })  : assert(selected != null),
        assert(icon != null),
        assert(label != null),
        assert(onTap != null),
        super(key: key);

  final Widget icon;
  final Widget label;
  final VoidCallback onTap;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 64.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        gradient: !selected ? null : HaipyGradients.blueGradient2,
      ),
      child: RawMaterialButton(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconTheme(
              data: IconThemeData(
                color: selected ? Colors.white : Colors.grey,
                size:
                    selected ? ScreenUtil().setSp(72) : ScreenUtil().setSp(64),
              ),
              child: icon,
            ),
            !selected
                ? DefaultTextStyle(
                    style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontSize: ScreenUtil().setSp(18),
                          fontWeight: FontWeight.w600,
                          color: selected ? Colors.white : Colors.grey,
                        ),
                    child: label,
                  )
                : const SizedBox(),
          ],
        ),
        onPressed: onTap,
      ),
    );
  }
}
