part of haipy_ui;

class HaipyTappableText extends StatelessWidget {
  const HaipyTappableText(
      {Key key,
      this.beforeText,
      this.tappableText,
      this.afterText,
      @required this.onTap})
      : super(key: key);

  final String afterText;
  final String beforeText;
  final String tappableText;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    TextStyle defaultTextStyle = DefaultTextStyle.of(context).style.merge(
          TextStyle(
            fontSize: ScreenUtil().setSp(32),
            fontWeight: FontWeight.w400,
          ),
        );

    return RichText(
      text: TextSpan(
        text: beforeText,
        style: defaultTextStyle,
        children: <TextSpan>[
          TextSpan(
            text: tappableText,
            style: defaultTextStyle.merge(
              TextStyle(
                color: HaipyColors.lightBlue,
              ),
            ),
            recognizer: TapGestureRecognizer()..onTap = onTap,
          ),
          afterText != null
              ? TextSpan(
                  text: afterText,
                  style: defaultTextStyle,
                )
              : null,
        ],
      ),
    );
  }
}
