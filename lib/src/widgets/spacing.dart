part of haipy_ui;

class HaipySpacing extends StatelessWidget {
  const HaipySpacing({
    Key key,
    this.direction = Axis.vertical,
    this.amount = 24.0,
  }) : super(key: key);


  const HaipySpacing.small({
    Key key,
    this.direction = Axis.vertical,
    this.amount = 16.0,
  }) : super(key: key);


  const HaipySpacing.large({
    Key key,
    this.direction = Axis.vertical,
    this.amount = 40.0,
  }) : super(key: key);

  final Axis direction;
  final double amount;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: direction == Axis.vertical ? amount : null,
      width: direction == Axis.horizontal ? amount : null,
    );
  }
}
