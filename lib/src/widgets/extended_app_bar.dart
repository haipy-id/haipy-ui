part of haipy_ui;

class HaipyExtendedAppBar extends StatelessWidget {
  const HaipyExtendedAppBar({
    Key key,
    this.backgroundColor,
    this.expandedHeight,
    this.background,
    this.floatingWidget,
    this.title,
  }) : super(key: key);

  final Color backgroundColor;
  final int expandedHeight;
  final Widget background;
  final Widget floatingWidget;
  final Widget title;

  @override
  Widget build(BuildContext context) {
    final double bottomCurveHeight = 12.0;
    final effectiveExpandedHeight = MediaQuery.of(context).padding.top +
        ScreenUtil().setHeight(expandedHeight ?? 300);
    final effectiveMinimumHeight =
        MediaQuery.of(context).padding.top + kToolbarHeight + bottomCurveHeight;

    return SliverPersistentHeader(
      delegate: MySliverAppBar(
        expandedHeight: effectiveExpandedHeight,
        minimumHeight: effectiveMinimumHeight,
        bottomCurveHeight: bottomCurveHeight,
        title: title,
        background: background,
        backgroundColor: backgroundColor,
        floatingWidget: floatingWidget,
      ),
      pinned: true,
    );
  }
}

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  const MySliverAppBar({
    Key key,
    this.backgroundColor,
    this.expandedHeight,
    this.minimumHeight,
    this.bottomCurveHeight,
    this.background,
    this.floatingWidget,
    this.title,
  });

  final Color backgroundColor;
  final double expandedHeight;
  final double minimumHeight;
  final double bottomCurveHeight;
  final Widget background;
  final Widget floatingWidget;
  final Widget title;

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        background ??
            Container(
              decoration: BoxDecoration(
                gradient: HaipyGradients.blueGradient2,
              ),
            ),
        Positioned(
          bottom: 0.0,
          child: Container(
            alignment: Alignment.center,
            height: bottomCurveHeight,
            width: MediaQuery.of(context).size.width,
            child: const SizedBox(),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16.0),
                topRight: Radius.circular(16.0),
              ),
              color: backgroundColor != null
                  ? Color.alphaBlend(
                      backgroundColor,
                      Theme.of(context).scaffoldBackgroundColor,
                    )
                  : Theme.of(context).scaffoldBackgroundColor,
            ),
            padding: const EdgeInsets.all(6.0),
          ),
        ),
        title != null
            ? SafeArea(
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      title,
                    ],
                  ),
                ),
              )
            : const SizedBox(),
        Positioned(
          top: expandedHeight / 2 - shrinkOffset - bottomCurveHeight,
          child: Opacity(
            opacity: 1 - (shrinkOffset / expandedHeight),
            child: SizedBox(
              height: expandedHeight,
              child: floatingWidget,
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => minimumHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
