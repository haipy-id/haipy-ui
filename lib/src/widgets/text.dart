part of haipy_ui;

class HaipyText extends StatelessWidget {
  const HaipyText(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 32,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.smallest(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 10,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.smaller(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 16,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.small(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 24,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.medium(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 40,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.big(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 48,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.bigger(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 56,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  const HaipyText.biggest(
    this.text, {
    Key key,
    this.color,
    this.fontWeight,
    this.fontSize: 64,
    this.textAlign = TextAlign.left,
  })  : assert(text != null, 'Text must not be null'),
        super(key: key);

  final Color color;
  final FontWeight fontWeight;
  final int fontSize;
  final String text;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    DefaultTextStyle defaultTextStyle = DefaultTextStyle.of(context);

    return Text(
      text,
      textAlign: textAlign,
      style: defaultTextStyle.style.merge(
        TextStyle(
          color: color,
          fontSize: ScreenUtil().setSp(fontSize),
          fontWeight: fontWeight ?? FontWeight.w400,
        ),
      ),
    );
  }
}
