part of haipy_ui;

class HaipyTabbedTabs extends StatefulWidget {
  HaipyTabbedTabs({
    Key key,
    @required this.tabTitles,
    @required this.onTabChanged,
    this.height = 48.0,
  })  : assert(tabTitles != null),
        super(key: key);

  final double height;
  final List<Widget> tabTitles;
  final ValueChanged<int> onTabChanged;

  @override
  _HaipyTabbedTabsState createState() => _HaipyTabbedTabsState();
}

class _HaipyTabbedTabsState extends State<HaipyTabbedTabs> {
  int _tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    double height =
        widget.height < 20.0 ? ScreenUtil().setHeight(40) : widget.height;
    List<Widget> tabTitles = List.generate(
      widget.tabTitles.length,
      (int index) {
        return _buildTabItem(
          context: context,
          item: widget.tabTitles[index],
          index: index,
          height: height,
          onSelected: (newIndex) {
            setState(() {
              _tabIndex = newIndex;
            });
            widget.onTabChanged(newIndex);
          },
        );
      },
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            color: HaipyColors.grey,
            borderRadius: BorderRadius.circular(36.0),
          ),
          child: Row(children: tabTitles),
        ),
      ],
    );
  }

  Widget _buildTabItem({
    BuildContext context,
    Widget item,
    int index,
    double height,
    ValueChanged<int> onSelected,
  }) {
    DefaultTextStyle defaultTextStyle = DefaultTextStyle.of(context);
    bool isActive = _tabIndex == index;

    return Expanded(
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: () => onSelected(index),
          child: Container(
            alignment: Alignment.center,
            height: height,
            padding: EdgeInsets.all(4.0),
            decoration: BoxDecoration(
              gradient: isActive ? HaipyGradients.blueGradient2 : null,
              borderRadius: BorderRadius.circular(36.0),
            ),
            child: DefaultTextStyle(
              style: defaultTextStyle.style.merge(
                TextStyle(
                    fontSize: ScreenUtil().setSp(32),
                    fontWeight: FontWeight.w400,
                    color: _tabIndex == index
                        ? Colors.white
                        : Theme.of(context).textTheme.caption.color),
              ),
              child: item,
            ),
          ),
        ),
      ),
    );
  }
}
