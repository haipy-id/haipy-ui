part of haipy_ui;

class PaddedContainer extends StatelessWidget {
  const PaddedContainer({
    Key key,
    this.decoration,
    this.height,
    this.width,
    this.padding: const EdgeInsets.all(16.0),
    @required this.child,
  }) : super(key: key);

  const PaddedContainer.small({
    Key key,
    this.decoration,
    this.height,
    this.width,
    this.padding: const EdgeInsets.all(8.0),
    @required this.child,
  }) : super(key: key);

  const PaddedContainer.large({
    Key key,
    this.decoration,
    this.height,
    this.width,
    this.padding: const EdgeInsets.all(24.0),
    @required this.child,
  }) : super(key: key);

  final BoxDecoration decoration;
  final double height;
  final double width;
  final EdgeInsets padding;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: child,
      decoration: decoration,
      height: height,
      padding: padding,
      width: width,
    );
  }
}
