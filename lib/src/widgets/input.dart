part of haipy_ui;

class HaipyInput extends StatelessWidget {
  const HaipyInput({
    Key key,
    this.disable = false,
    this.inverted = false,
    this.obscureText = false,
    this.color,
    this.errorText,
    this.labelText,
    this.placeholder,
    this.controller,
    this.inputType,
    this.onChanged,
    this.onSubmitted,
    this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  const HaipyInput.inverted({
    Key key,
    this.disable = false,
    this.inverted = true,
    this.obscureText = false,
    this.color,
    this.errorText,
    this.labelText,
    this.placeholder,
    this.controller,
    this.inputType,
    this.onChanged,
    this.onSubmitted,
    this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  final bool disable;
  final bool inverted;
  final bool obscureText;
  final Color color;
  final String errorText;
  final String labelText;
  final String placeholder;
  final TextEditingController controller;
  final TextInputType inputType;
  final ValueChanged<String> onChanged;
  final ValueChanged<String> onSubmitted;
  final Widget prefixIcon;
  final Widget suffixIcon;

  @override
  Widget build(BuildContext context) {
    InputDecoration _inputDecoration = InputDecoration(
      isDense: true,
      errorText: errorText,
      labelText: labelText,
      prefixIcon: prefixIcon,
      suffixIcon: suffixIcon,
    );

    TextStyle _textStyle = TextStyle(
      fontSize: ScreenUtil().setSp(32),
      fontWeight: FontWeight.w400,
    );

    InputBorder _border = inverted
        ? OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black54),
            borderRadius: BorderRadius.circular(36.0),
          )
        : UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.black54),
            borderRadius: BorderRadius.circular(36.0),
          );

    InputDecorationTheme _inputDecorationTheme = InputDecorationTheme(
      floatingLabelBehavior: FloatingLabelBehavior.never,
      labelStyle: _textStyle.copyWith(
        color: Colors.black87,
      ),
      hintStyle: _textStyle.copyWith(
        color: Colors.black87,
      ),
      errorStyle: _textStyle.copyWith(
        fontSize: ScreenUtil().setSp(24),
        color: Colors.red,
      ),
      border: _border,
      enabledBorder: _border,
      focusedBorder: _border.copyWith(
        borderSide: BorderSide(
          color: Theme.of(context).accentColor,
          width: 2.0,
        ),
      ),
    );

    return Theme(
      data: Theme.of(context).copyWith(
        inputDecorationTheme: _inputDecorationTheme,
      ),
      child: TextField(
        enabled: !disable,
        autocorrect: false,
        obscureText: obscureText,
        decoration: _inputDecoration,
        keyboardType: inputType,
        onChanged: onChanged,
        onSubmitted: onSubmitted,
        style: _textStyle,
      ),
    );
  }
}
