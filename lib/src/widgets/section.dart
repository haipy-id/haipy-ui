part of haipy_ui;

class Section extends StatelessWidget {
  const Section({
    Key key,
    this.topMargin: 0.0,
    this.title,
    this.action,
    @required this.child,
  }) : super(key: key);

  const Section.smallTopMargin({
    Key key,
    this.topMargin: 16.0,
    this.title,
    this.action,
    @required this.child,
  }) : super(key: key);

  const Section.mediumTopMargin({
    Key key,
    this.topMargin: 24.0,
    this.title,
    this.action,
    @required this.child,
  }) : super(key: key);

  const Section.largeTopMargin({
    Key key,
    this.topMargin: 48.0,
    this.title,
    this.action,
    @required this.child,
  }) : super(key: key);

  final double topMargin;
  final String title;
  final Widget action;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: topMargin),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (title != null)
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                HaipyText.medium(title),
                action ?? const SizedBox(),
              ],
            ),
          child,
        ],
      ),
    );
  }
}
