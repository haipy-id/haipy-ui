part of haipy_ui;

class HaipyBottomTabView extends StatefulWidget {
  HaipyBottomTabView({
    Key key,
    @required List<Widget> pages,
    @required List<Widget> tabTitles,
    this.height = 40.0,
  })  : assert(pages != null),
        assert(tabTitles != null),
        _pages = pages,
        _tabTitles = tabTitles,
        super(key: key);

  final double height;
  final List<Widget> _pages;
  final List<Widget> _tabTitles;

  @override
  _HaipyBottomTabViewState createState() => _HaipyBottomTabViewState();
}

class _HaipyBottomTabViewState extends State<HaipyBottomTabView> {
  int _tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    double height =
        widget.height < 20.0 ? ScreenUtil().setHeight(40) : widget.height;
    List<Widget> tabTitles =
        List.generate(widget._tabTitles.length, (int index) {
      return _buildTabItem(
        context: context,
        item: widget._tabTitles[index],
        index: index,
        height: height,
        onSelected: (newIndex) {
          setState(() {
            _tabIndex = newIndex;
          });
        },
      );
    });

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          margin: EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            color: Theme.of(context).dividerColor,
            borderRadius: BorderRadius.circular(height * 0.1),
          ),
          child: Row(children: tabTitles),
        ),
        AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          child: widget._pages[_tabIndex],
        ),
      ],
    );
  }

  Widget _buildTabItem({
    BuildContext context,
    Widget item,
    int index,
    double height,
    ValueChanged<int> onSelected,
  }) {
    bool isActive = _tabIndex == index;

    return Expanded(
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          onTap: () => onSelected(index),
          child: Container(
            alignment: Alignment.center,
            height: height,
            padding: EdgeInsets.all(4.0),
            decoration: BoxDecoration(
              color: isActive ? null : Theme.of(context).dividerColor,
              gradient: isActive ? HaipyGradients.blueGradient1 : null,
              borderRadius: _tabIndex == index
                  ? BorderRadius.circular(height * 0.1)
                  : null,
              boxShadow: isActive
                  ? [
                      BoxShadow(
                        color: Color(0x88000000),
                        blurRadius: 4.0,
                        offset: Offset(0.0, 0.0),
                      ),
                    ]
                  : null,
            ),
            child: DefaultTextStyle(
                style: Theme.of(context).textTheme.bodyText2.copyWith(
                    fontSize: ScreenUtil().setSp(32),
                    fontWeight: FontWeight.w600,
                    color: _tabIndex == index
                        ? Colors.white
                        : Theme.of(context).textTheme.caption.color),
                child: item),
          ),
        ),
      ),
    );
  }
}
