part of haipy_ui;

class HaipyTabbedView extends StatefulWidget {
  HaipyTabbedView({
    Key key,
    @required List<Widget> pages,
    @required List<Widget> tabTitles,
  })  : assert(pages != null),
        assert(tabTitles != null),
        _pages = pages,
        _tabTitles = tabTitles,
        super(key: key);

  final List<Widget> _pages;
  final List<Widget> _tabTitles;

  @override
  _HaipyTabbedViewState createState() => _HaipyTabbedViewState();
}

class _HaipyTabbedViewState extends State<HaipyTabbedView> {
  int _tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        HaipyTabbedTabs(
          tabTitles: widget._tabTitles,
          onTabChanged: (tabIndex) => setState(() => _tabIndex = tabIndex),
        ),
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 300),
          child: widget._pages[_tabIndex],
        ),
      ],
    );
  }
}
