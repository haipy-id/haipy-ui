part of haipy_ui;

class HaipySubHeaderDelegate extends SliverPersistentHeaderDelegate {
  final Widget child;

  HaipySubHeaderDelegate({@required this.child});

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    return child;
  }

  @override
  double get minExtent => 80.0;

  @override
  double get maxExtent => 80.0;

  @override
  bool shouldRebuild(HaipySubHeaderDelegate oldDelegate) => false;
}