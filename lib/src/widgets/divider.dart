part of haipy_ui;

class HaipyDivider extends StatelessWidget {
  final double height;
  final String label;

  const HaipyDivider({
    Key key,
    this.label,
    this.height = 24.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Flexible(
        child: new Container(
            margin: const EdgeInsets.only(left: 8.0, right: 16.0),
            child: Divider(
              color: Colors.black54,
              height: height,
            )),
      ),
      label != null ? Text(label) : const SizedBox(),
      Flexible(
        child: new Container(
            margin: const EdgeInsets.only(left: 16.0, right: 8.0),
            child: Divider(
              color: Colors.black54,
              height: height,
            )),
      ),
    ]);
  }
}
