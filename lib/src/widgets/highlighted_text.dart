part of haipy_ui;

class HaipyHighlightedText extends StatelessWidget {
  const HaipyHighlightedText({
    Key key,
    this.highlighColor = Colors.amberAccent,
    this.textColor,
    this.size = 32,
    this.afterText,
    this.beforeText,
    this.highlightedText,
    this.textAlign = TextAlign.left,
  }) : super(key: key);

  final Color highlighColor;
  final Color textColor;
  final int size;
  final String afterText;
  final String beforeText;
  final String highlightedText;
  final TextAlign textAlign;

  @override
  Widget build(BuildContext context) {
    TextStyle defaultTextStyle = DefaultTextStyle.of(context).style.merge(
          TextStyle(
            color: textColor,
            fontSize: ScreenUtil().setSp(size),
            fontWeight: FontWeight.w400,
          ),
        );

    return RichText(
      textAlign: textAlign,
      text: TextSpan(
        text: beforeText,
        style: defaultTextStyle,
        children: <TextSpan>[
          TextSpan(
            text: highlightedText,
            style: defaultTextStyle.merge(
              TextStyle(
                backgroundColor: highlighColor,
              ),
            ),
          ),
          afterText != null
              ? TextSpan(
                  text: afterText,
                  style: defaultTextStyle,
                )
              : null,
        ],
      ),
    );
  }
}
