part of haipy_ui;

class HaipyPage extends StatelessWidget {
  const HaipyPage({
    Key key,
    this.hideBackButton = false,
    this.extendedAppBar = false,
    this.backgroundColor,
    this.appBarExpandedHeight,
    this.actions,
    this.slivers,
    this.appBarBackground,
    this.appBarTitle,
    this.bottomNavigationBar,
    this.child,
    this.floatingChild,
    this.pageTitle,
  })  : assert(child != null || slivers != null, 'Can\'t add empty pages'),
        assert(extendedAppBar || floatingChild == null, 'Floating Widget can only be used with extendedAppBar'),
        super(key: key);

  final bool hideBackButton;
  final bool extendedAppBar;
  final Color backgroundColor;
  final num appBarExpandedHeight;
  final List<Widget> actions;
  final List<Widget> slivers;
  final Widget appBarBackground;
  final Widget appBarTitle;
  final Widget bottomNavigationBar;
  final Widget child;
  final Widget floatingChild;
  final Widget pageTitle;

  Widget _buildPageTitle({@required Widget child}) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: HaipySubHeaderDelegate(child: child),
    );
  }

  @override
  Widget build(BuildContext context) {
    const headerPadding = EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0);

    return Scaffold(
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(),
        slivers: <Widget>[
          extendedAppBar
              ? HaipyExtendedAppBar(
                  expandedHeight: appBarExpandedHeight,
                  background: appBarBackground,
                  backgroundColor: backgroundColor,
                  title: appBarTitle,
                  floatingWidget: floatingChild,
                )
              : HaipyAppBar(
                  hideBackButton: hideBackButton,
                  backgroundColor: backgroundColor,
                  expandedHeight: appBarExpandedHeight,
                  actions: actions,
                  background: appBarBackground,
                  title: appBarTitle,
                ),
          if (pageTitle != null)
            _buildPageTitle(
              child: Container(
                alignment: Alignment.center,
                color: Theme.of(context).cardColor,
                padding: headerPadding,
                child: pageTitle,
              ),
            ),
        ]
            .followedBy(slivers ?? [])
            .followedBy(
              child != null
                  ? [
                      SliverPadding(
                        padding: pageTitle != null
                            ? headerPadding
                            : const EdgeInsets.all(16.0),
                        sliver: SliverToBoxAdapter(child: child),
                      ),
                    ]
                  : [],
            )
            .toList(),
      ),
      bottomNavigationBar: bottomNavigationBar,
    );
  }
}
