# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.5] - 2020-09-06
### Changed
- Start using a better changelog file format.
- Use new font (Source Sans Pro).
- Set the roundness of input fields.
- Set enable/disable state of input fields.

### Fixed
- Fix button text overflowing the container

## [0.0.4] - 2020-08-21
### Added
- Add divider and spacing widgets.

### Fixed
- Fix style and logic for input.
- Adjust text styles.

## [0.0.1] - 2020-08-14
### Added
- Initial files
